import { LitElement, html, css } from 'lit-element';

class BookList  extends LitElement {



  static get properties() {
    return {
        data: {type: Object},

    };
  }

  constructor() {
    super();
    this.data = {"books": [] };
    this.cargarDatos();
  }

  render() {
    return html`
      <div>
        ${this.data.books.map( (b) => html `<div>${b.titulo} - ${b.autor}</div>`) }
      </div>
    `;
  }

  cargarDatos() {
    fetch("http://localhost:3392/books")
      .then(response => {
          console.log(response);
          if (!response.ok) { throw response;}
          return response.json();
      })
      .then(data => {
          this.data = data;
          console.log(data);
      })
      .catch(error => {
          alert("Problemas con el fecth: " + error);
      })
    }
}





customElements.define('book-list', BookList);