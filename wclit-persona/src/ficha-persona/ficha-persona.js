import { LitElement, html, css } from 'lit-element';
import {OrigenPersona } from '../origen-persona/origen-persona.js';

class FichaPersona  extends LitElement {

  static get styles() {
    return css`
            div {
                border: solid 1px;
                border-radius: 10px;
                padding: 10px;
                margin: 10px;
            }
    `;
  }

  static get properties() {
    return {
        nombre: {type: String},
        apellidos: {type: String},
        aniosAntiguedad: {type: Number},
        foto: {type: Object},
        nivel: {type: String},
        bg: {type:String}
    };
  }

  constructor() {
    super();
    this.nombre = "Marcos";
    this.apellidos = "Goxcon";
    this.aniosAntiguedad = 2;
    this.foto = {
        src: "src/img/persona.jpg",
        alt:"Mi foto"
    }
    this.bg = "aliceblue";
  }

  render() {
    return html`
      <div style="width:400px;background-color:${this.bg};"> 
        <label for="inombre"> Nombre </label>
        <input type="text" id="inombre" name="inombre" value="${this.nombre}" @input= "${this.updateNombre}">
        <br>
        <label for="iapellidos"> Apellidos </label>
        <input type="text" id="iapellidos" name="iapellidos" value="${this.apellidos}">
        <br>
        <label for="iantiguedad"> Antiguedad </label>
        <input type="number" id="iantiguedad" name="iantiguedad" value="${this.aniosAntiguedad}" 
                            @input="${this.updateAntiguedad}">
        <br>
        <label for="inivel"> Nivel </label>
        <input type="text" id="inivel" name="inivel" value="${this.nivel}" disabled>
        <br>
        
        <origen-persona @origen-set="${this.origenChange}"></origen-persona>

        <img src="${this.foto.src}" height="200" width="200" alt="${this.foto.alt}" />
      </div>
    `;
  }

  updated(changedProperties){
      if (changedProperties.has("nombre")) {
          console.log("Propiedad nombre cambiada. Valor enterior: " + 
                      changedProperties.get("nombre") + " Valor nuevo: " + this.nombre);
      }
      if (changedProperties.has("aniosAntiguedad")) {
          this.actualizarNivel();
      }
  }

  updateNombre(e){
      this.nombre = e.target.value;
  }

  updateAntiguedad(e){
     this.aniosAntiguedad =  e.target.value; 

  }
actualizarNivel(){
    if (this.aniosAntiguedad >= 7) {
        this.nivel = "Lider";
    } else if (this.aniosAntiguedad >= 5){
      this.nivel = "Senior";
    } else if (this.aniosAntiguedad >= 3){
      this.nivel = "Team";
} else{
  this.nivel = "Junior";
}
}

origenChange(e){
    var origen =  e.detail.message;
    if (origen === "USA") {
        this.bg = "pink";
    } else if (origen === "México"){
        this.bg = "lightgreen"
    } else if (origen === "Rusia"){
        this.bg = "lightyellow"
    }
}

}




customElements.define('ficha-persona', FichaPersona);